package test;

import main.java.encephalon.client.HttpApiClient;

public class TestFramework
{
    private static HttpApiClient client = new HttpApiClient();

    public static ResponseObject MakeRequestGET(String protocol, String host, String port, String schema, String... params) throws Exception
    {
        client.makeRequestGet(protocol, null, host + ":" + port, schema, params);
        
        return GetResponse();
    }
    
    public static ResponseObject MakeRequestPOST(String protocol, String host, String port, String schema, String request, String... params) throws Exception
    {
        client.makeRequestPost(protocol, null, host + ":" + port, schema, request, params);
        
        return GetResponse();
    }
    
    public static ResponseObject MakeRequestPUT(String protocol, String host, String port, String schema, String request, String... params) throws Exception
    {
        client.makeRequestPut(protocol, null, host + ":" + port, schema, request, params);
        
        return GetResponse();
    }
    
    public static ResponseObject MakeRequestDELETE(String protocol, String host, String port, String schema, String... params) throws Exception
    {
        client.makeRequestDelete(protocol, null, host + ":" + port, schema, params);
        
        return GetResponse();
    }
    
    private static ResponseObject GetResponse() throws Exception
    {
        return new ResponseObject(client.getResponseCode(), client.readResponse(), client.getHeaders());
    }
}
