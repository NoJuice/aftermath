package test;

import java.util.List;
import java.util.Map;

public class ResponseObject
{
    private final int statusCode;
    private final String responseBody;
    private final Map<String, List<String>> headers;

    ResponseObject(int statusCode, String responseBody, Map<String, List<String>> headers)
    {
        this.statusCode = statusCode;
        this.responseBody = responseBody;
        this.headers = headers;
    }
}
