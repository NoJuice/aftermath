package main.java.aftermath.sample;

import java.util.HashSet;

import org.apache.log4j.BasicConfigurator;
import org.eclipse.jetty.alpn.server.ALPNServerConnectionFactory;
import org.eclipse.jetty.http2.server.HTTP2ServerConnectionFactory;
import org.eclipse.jetty.http3.server.HTTP3ServerConnectionFactory;
import org.eclipse.jetty.http3.server.HTTP3ServerConnector;
import org.eclipse.jetty.server.HttpConfiguration;
import org.eclipse.jetty.server.HttpConnectionFactory;
import org.eclipse.jetty.server.SecureRequestCustomizer;
import org.eclipse.jetty.server.ServerConnector;
import org.eclipse.jetty.server.handler.gzip.GzipHandler;
import org.eclipse.jetty.util.ssl.SslContextFactory;
import org.eclipse.jetty.util.thread.QueuedThreadPool;

import main.java.aftermath.handlers.*;
import main.java.aftermath.server.*;
import main.java.encephalon.API.scope.Scope;
import main.java.encephalon.cluster.ClusteringManager;
import main.java.encephalon.logger.Logger;
import main.java.encephalon.server.DefaultHandler;

public class StartServer extends main.java.encephalon.sample.StartServer
{    
    public static void main(String[] args) throws Exception
    {
        try
        {
            // Comment out to disable log4j logger
            AftermathServer aftermath = AftermathServer.getInstance();
            
            boolean fourJLogger = aftermath.getProperty("4jLogger.enable", false, "Enable 4j Logger.");
            String httpModes = aftermath.getProperty("server.http.modes", "h2", "Http modes");
            String keyStorePath = aftermath.getProperty("ssl.keyStore", "SSL Keystore file.");
            String keyStorePass = aftermath.getProperty("ssl.keyStore.password", "", "SSL Keystore password.");
            String trustStorePath = aftermath.getProperty("ssl.trustStore", "SSL Truststore file.");
            String trustStorePass = aftermath.getProperty("ssl.trustStore.password", "", "SSL Truststore password.");
            Integer publicPort = aftermath.getProperty("server.port.public", 8083, "Public Port.");
            Integer privatePort = aftermath.getProperty("server.port.private", 8082, "Private Port.");
            Integer maintenancePort = aftermath.getProperty("server.port.maintenance", 8081, "Maintenance Port.");
            Integer publicSSLPort = aftermath.getProperty("server.ssl.port.public", 8443, "SSL Public Port.");
            Integer publicPortQueueSize = aftermath.getProperty("server.port.public.queue.size", 10, "Public port queue size.");
            Integer privatePortQueueSize = aftermath.getProperty("server.port.private.queue.size", 5, "Private port queue size.");
            Integer maintenancePortQueueSize = aftermath.getProperty("server.port.maintenance.queue.size", 2, "Maintenance port queue size.");
            Integer publicSSLPortQueueSize = aftermath.getProperty("server.ssl.port.public.queue.size", 1000, "SSL Public port queue size.");
            
            if(fourJLogger)
            {
                BasicConfigurator.configure();
            }

            aftermath.initializeMap();
            
            ClusteringManager.init(aftermath.getAftermathController().getEdgeData());

            GzipHandler gzipHandler = new GzipHandler();
            gzipHandler.setHandler(new DefaultHandler());
            aftermath.setHandler(gzipHandler);
            
            registerAftermathHandlers(aftermath);
            registerEncephalonHandlers(aftermath);

            HttpConfiguration httpConfig = new HttpConfiguration();
            httpConfig.addCustomizer(new SecureRequestCustomizer());

            HttpConnectionFactory http1ConnectionFactory = new HttpConnectionFactory(httpConfig);
            HTTP2ServerConnectionFactory http2ConnectionFactory = new HTTP2ServerConnectionFactory(httpConfig);
            HTTP3ServerConnectionFactory http3ConnectionFactory = new HTTP3ServerConnectionFactory(httpConfig);
            ALPNServerConnectionFactory alpn = new ALPNServerConnectionFactory(httpModes);
            
            ServerConnector publicConnector = new ServerConnector(aftermath, publicPortQueueSize, -1, http1ConnectionFactory, http2ConnectionFactory);
            publicConnector.setName(Scope.publicPort.name());
            publicConnector.setPort(publicPort);
            aftermath.addConnector(publicConnector);
            
            ServerConnector privateConnector = new ServerConnector(aftermath, privatePortQueueSize, -1, http1ConnectionFactory, http2ConnectionFactory);
            privateConnector.setName(Scope.privatePort.name());
            privateConnector.setPort(privatePort);
            aftermath.addConnector(privateConnector);

            ServerConnector maintenanceConnector = new ServerConnector(aftermath, maintenancePortQueueSize, -1, http1ConnectionFactory, http2ConnectionFactory);
            maintenanceConnector.setName(Scope.maintenancePort.name());
            maintenanceConnector.setPort(maintenancePort);
            aftermath.addConnector(maintenanceConnector);
            
            if(keyStorePath != null)
            {
                try
                {
                    Logger.Log("SSL", "Trust Store Path: " + trustStorePath);
                    SslContextFactory.Server sslContext = new SslContextFactory.Server();
                    
                    sslContext.setIncludeProtocols("TLSv1.3", "TLSv1.2");
                    sslContext.setExcludeCipherSuites("TLS_DHE_RSA_WITH_AES_256_GCM_SHA384",   // Weak cipher suites
                            "TLS_DHE_RSA_WITH_CHACHA20_POLY1305_SHA256",
                            "TLS_DHE_RSA_WITH_AES_128_GCM_SHA256",
                            "TLS_ECDHE_RSA_WITH_AES_256_CBC_SHA384",
                            "TLS_ECDHE_RSA_WITH_AES_128_CBC_SHA256",
                            "TLS_DHE_RSA_WITH_AES_256_CBC_SHA256",
                            "TLS_DHE_RSA_WITH_AES_128_CBC_SHA256",
                            "TLS_ECDHE_ECDSA_WITH_AES_256_CBC_SHA",
                            "TLS_ECDHE_RSA_WITH_AES_256_CBC_SHA",
                            "TLS_ECDHE_ECDSA_WITH_AES_128_CBC_SHA",
                            "TLS_ECDHE_RSA_WITH_AES_128_CBC_SHA",
                            "TLS_DHE_RSA_WITH_AES_256_CBC_SHA",
                            "TLS_DHE_DSS_WITH_AES_256_CBC_SHA",
                            "TLS_DHE_RSA_WITH_AES_128_CBC_SHA",
                            "TLS_DHE_DSS_WITH_AES_128_CBC_SHA",
                            "TLS_ECDH_ECDSA_WITH_AES_256_CBC_SHA",
                            "TLS_ECDH_RSA_WITH_AES_256_CBC_SHA",
                            "TLS_ECDH_ECDSA_WITH_AES_128_CBC_SHA",
                            "TLS_ECDH_RSA_WITH_AES_128_CBC_SHA",
                            "TLS_RSA_WITH_AES_256_GCM_SHA384",
                            "TLS_RSA_WITH_AES_128_GCM_SHA256",
                            "TLS_RSA_WITH_AES_256_CBC_SHA256",
                            "TLS_RSA_WITH_AES_128_CBC_SHA256",
                            "TLS_RSA_WITH_AES_256_CBC_SHA",
                            "TLS_RSA_WITH_AES_256_CBC_SHA",
                            "TLS_RSA_WITH_AES_128_CBC_SHA",
                            "TLS_RSA_WITH_AES_128_CBC_SHA");
                    sslContext.setKeyStorePath(keyStorePath);
                    sslContext.setKeyStorePassword(keyStorePass);
                    sslContext.setTrustStorePath(trustStorePath);
                    sslContext.setTrustStorePassword(trustStorePass);

                    // HTTP3ServerConnector h3Connector = new HTTP3ServerConnector(aftermath, sslContext, alpn, http3ConnectionFactory);
                    // h3Connector.setName(Scope.publicSSLPort.name());
                    // h3Connector.setPort(8444);
                    // aftermath.addConnector(h3Connector);
                    
                    ServerConnector sslConnector = new ServerConnector(aftermath, sslContext, alpn, http1ConnectionFactory, http2ConnectionFactory);
                    sslConnector.setName(Scope.publicSSLPort.name());
                    sslConnector.setAcceptQueueSize(publicSSLPortQueueSize);
                    sslConnector.setPort(publicSSLPort);
                    aftermath.addConnector(sslConnector);
                }
                catch (Exception e)
                {
                    Logger.Log("SSL-FAIL", "Failed to setup SSL");
                    // Log failure to load keystore
                }
            }
            else
            {
                Logger.Log("SSL-ABSENT", "SSL Not Configured.  Skipping");
            }

            QueuedThreadPool pool = (QueuedThreadPool)aftermath.getThreadPool();
            pool.setMaxThreads(1200);
            
            aftermath.start();
            aftermath.join();
        }
        catch (Throwable t)
        {
            HashSet<Integer> exceptionHash = new HashSet<Integer>();
            String tabSt = "  ";

            while (t != null && !exceptionHash.contains(t.hashCode()))
            {
                StringBuilder sb = new StringBuilder();
                StackTraceElement[] elements = t.getStackTrace();
                sb.append("  Message: " + t.getMessage() + "\r\n");

                for (StackTraceElement element : elements)
                {
                    sb.append(tabSt + element.getClassName() + "." + element.getMethodName() + "("
                            + element.getFileName() + ":" + element.getLineNumber() + ")\r\n");
                }

                tabSt += "  ";
                exceptionHash.add(t.hashCode());
                System.err.println(sb.toString());
                t = t.getCause();
            }
        }
    }

    public static void registerAftermathHandlers(AftermathServer server) throws Exception
    {
        server.registerUri("/aftermath", "aftermath", new AftermathHandler());
        // server.registerUri("/", "wcencephalon", new
        // WCEncephalonHandler((AftermathServer)server));
    }
}